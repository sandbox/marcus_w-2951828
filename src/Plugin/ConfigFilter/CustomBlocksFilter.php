<?php

namespace Drupal\config_ignore_custom_blocks\Plugin\ConfigFilter;

use Drupal\config_filter\Plugin\ConfigFilterBase;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a custom block placement filter.
 *
 * @ConfigFilter(
 *   id = "custom_blocks_filter",
 *   label = @Translation("Filters custom blocks from configuration"),
 *   status = TRUE,
 *   storages = {"config.storage.sync"},
 * )
 */
class CustomBlocksFilter extends ConfigFilterBase implements ContainerFactoryPluginInterface {

  /**
   * The active configuration storage.
   *
   * @var \Drupal\Core\Config\StorageInterface
   */
  protected $active;

  /**
   * Constructs a new CustomBlockFilter.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\StorageInterface $active
   *   The active configuration store with the configuration on the site.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, StorageInterface $active) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->active = $active;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.storage')
    );
  }

  /**
   * Read multiple from the active storage.
   *
   * @param array $names
   *   The names of the configuration to read.
   *
   * @return array
   *   The new data.
   */
  protected function activeReadMultiple(array $names) {
    $filtered_data = [];
    foreach ($names as $name) {
      $filtered_data[$name] = $this->active->read($name);
    }

    return $filtered_data;
  }

  /**
   * Checks if the name matches a custom block.
   *
   * @param string $name
   *   The configuration name.
   *
   * @return bool
   *   TRUE if it matches.
   */
  public function isCustomBlock($name) {
    if (substr($name, 0, 11) !== 'block.block') {
      return FALSE;
    }
    $data = $this->active->read($name);
    if (isset($data['plugin'])) {
      return substr($data['plugin'], 0, 13) === 'block_content';
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function filterRead($name, $data) {
    if ($this->isCustomBlock($name)) {
      return $this->active->read($name);
    }
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function filterWrite($name, array $data) {
    return $this->isCustomBlock($name) ? NULL : $data;
  }

  /**
   * {@inheritdoc}
   */
  public function filterExists($name, $exists) {
    // A name exists if it is ignored and exists in the active storage.
    return $exists || ($this->isCustomBlock($name) && $this->active->exists($name));
  }

  /**
   * {@inheritdoc}
   */
  public function filterReadMultiple(array $names, array $data) {
    // Limit the names which are read from the active storage.
    $names = array_filter($names, [$this, 'isCustomBlock']);
    $active_data = $this->activeReadMultiple($names);

    // Return the data with merged in active data.
    return array_merge($data, $active_data);
  }

  /**
   * {@inheritdoc}
   */
  public function filterListAll($prefix, array $data) {
    $active_names = $this->active->listAll($prefix);
    // Filter out only ignored config names.
    $active_names = array_filter($active_names, [$this, 'isCustomBlock']);

    // Return the data with the active names which are ignored merged in.
    return array_unique(array_merge($data, $active_names));
  }

  /**
   * {@inheritdoc}
   */
  public function filterCreateCollection($collection) {
    return new static($this->configuration, $this->pluginId, $this->pluginDefinition, $this->active->createCollection($collection));
  }

  /**
   * {@inheritdoc}
   */
  public function filterGetAllCollectionNames(array $collections) {
    // Add active collection names as there could be ignored config in them.
    return array_merge($collections, $this->active->getAllCollectionNames());
  }

}
